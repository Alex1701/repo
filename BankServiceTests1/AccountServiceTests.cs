﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankService;
using BankService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BankService.Tests
{
    [TestClass()]
    public class AccountServiceTests
    {
        const int NUMBER_OF_USERS = 50;
        const int NUMBER_OF_THREADS = 10;

        const decimal CREDIT_VALUE = 500;
        const decimal DEBIT_VALUE = -10;
        const decimal EXPECTED_BALANCE = (CREDIT_VALUE - DEBIT_VALUE) * NUMBER_OF_THREADS;

        int[] userID;
        private AccountService target;

        [TestMethod()]
        public void WebServiceTest()
        {
            target = new AccountService();
            target.AccountClearAll();

            for (int i = 0; i < NUMBER_OF_USERS; i++)
            {
                target.AccountRegister("Name" + i.ToString(), "LastName" + i.ToString(), "FatherName" + i.ToString(), "1990-01-01");
            }
            userID = target.AccountGetUserID();
            AccountCreditingTest();

            for (int i = 0; i < NUMBER_OF_USERS; i++)
                Assert.AreEqual(EXPECTED_BALANCE, target.AccountCheckBalance(userID[i]));
        }
        private void AccountCreditingTest()
        {
            Thread[] moneyThread = new Thread[NUMBER_OF_THREADS];

            for (int i = 0; i < NUMBER_OF_THREADS; i++)
            {
                moneyThread[i] = new Thread(new ThreadStart(AccountCrediting));
                moneyThread[i].Start();
            }

            int wait = 1;
            while (wait != 0)
            {
                wait = 0;
                foreach (Thread t in moneyThread)
                    if (t.IsAlive)
                        wait++;
            }
        }
        private object locker = new object();
        private void AccountCrediting()
        {
            lock (locker)
            {
                for (int i = 0; i < userID.Length; i++)
                    target.AccountCredit(userID[i], CREDIT_VALUE);
                for (int i = 0; i < userID.Length; i++)
                    target.AccountCredit(userID[i], DEBIT_VALUE);
            }
        }
    }
}