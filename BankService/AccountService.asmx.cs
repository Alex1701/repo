﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading.Tasks;
using BankService.Models;

namespace BankService
{
    /// <summary>
    /// Summary description for AccountService
    /// </summary>
    [WebService(Namespace = "BankService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AccountService : System.Web.Services.WebService
    {
        public AccountContext accDB = new AccountContext();

        [WebMethod]
        public void AccountRegister(string name, string lastName, string fatherName, string birth)
        {
            Account newAcc = new Account()
            {
                Name = name,
                LastName = lastName,
                FatherName = fatherName,
                Birth = DateTime.Parse(birth),
                Balance = 0
            };
            accDB.Accounts.Add(newAcc);
            accDB.SaveChanges();
        }
        [WebMethod]
        public decimal AccountCheckBalance(int id)
        {
            Account acc = accDB.Accounts.Find(id);
            if (acc != null)
                return acc.Balance;
            else
                return -1;
        }
        [WebMethod]
        public async Task<int> AccountCredit(int id, decimal value)
        {
            Account acc = accDB.Accounts.Find(id);
            if (acc != null)
                acc.Balance += value;
            await accDB.SaveChangesAsync();
            return 0;
        }
        public void AccountClearAll()
        {
            foreach (Account acc in accDB.Accounts)
            {
                accDB.Accounts.Remove(acc);
            }
            accDB.SaveChanges();
        }
        public IQueryable<Account> AccountGetAll()
        {
            return accDB.Accounts;
        }
        public int[] AccountGetUserID()
        {
            int[] userID = new int[accDB.Accounts.Count()];
            int i = 0;
            foreach (Account acc in accDB.Accounts)
                userID[i++] = acc.ID;
            return userID;
        }
    }
}
