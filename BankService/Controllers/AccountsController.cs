﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BankService;
using BankService.Models;

namespace BankService.Controllers
{
    public class AccountsController : ApiController
    {
        //        private AccountContext db = new AccountContext();
        private readonly AccountService service = new AccountService();

        // GET: api/Accounts
        public IQueryable<Account> GetAccounts()
        {
            return service.AccountGetAll();// service.accDB.Accounts;
        }

        // GET: api/Accounts/5
        [ResponseType(typeof(Account))]
        public decimal GetAccountBalance(int id)
        {
            decimal balance = service.AccountCheckBalance(id);
            return balance;
        }
        
        // PUT: api/Accounts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccountCredit(int id, decimal value)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int[] IDs = service.AccountGetUserID();
            if (!IDs.Contains(id))
            {
                return BadRequest();
            }

            Account account = service.accDB.Accounts.Find(id);

            bool accModified = false;
            while (!accModified)
            {
                try
                {
                    service.accDB.Entry(account).State = EntityState.Modified;
                    await service.AccountCredit(id, value);
                    accModified = true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccountExists(id))
                    {
                        return NotFound();
                    }
                }
            }

            return StatusCode(HttpStatusCode.OK);
        }

        // POST: api/Accounts
        [ResponseType(typeof(Account))]
        public async Task<IHttpActionResult> PostAccount(Account account)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            service.accDB.Accounts.Add(account);
            await service.accDB.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = account.ID }, account);
        }

        // DELETE: api/Accounts/5
        [ResponseType(typeof(Account))]
        public async Task<IHttpActionResult> DeleteAccount(int id)
        {
            
            Account account = await service.accDB.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            service.accDB.Accounts.Remove(account);
            await service.accDB.SaveChangesAsync();

            return Ok(account);
        }

        protected override void Dispose(bool disposing)
        {
            
            if (disposing)
            {
                service.accDB.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountExists(int id)
        {
            if (service.accDB.Accounts.Find(id) != null)
                return true;
            else
                return false;
        }
    }
}