﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BankService.Models
{
    public class AccountContext : DbContext
    { 
        public DbSet<Account> Accounts { get; set; }
    }
    public class Account
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public DateTime Birth { get; set; }
        public decimal Balance { get; set; }
    }
}